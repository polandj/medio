# Copyright(c) 2016-2021 Jonathan Poland
VERSION = 1.0
UI := $(shell find WIZARD_UIFILES -type f -print)
SCRIPTS := $(shell find scripts -type f -print)
PACKAGE_FILES := $(shell ./package.sh)

all: package.tgz Medio-${VERSION}.spk

package.tgz: ${PACKAGE_FILES}
	# Use python itself as a basic syntax linter
	PYTHONPATH=package python3 -c "from package import medio"
	@echo 'Remaking package tarball'
	@tar czf package.tgz -C package $(subst package,.,${PACKAGE_FILES})

Medio-${VERSION}.spk: INFO PACKAGE_ICON.PNG LICENSE ${UI} ${SCRIPTS} package.tgz
	sed -i 's/version=.*/version="${VERSION}"/g' INFO
	tar cf Medio-${VERSION}.spk INFO PACKAGE_ICON* LICENSE WIZARD_UIFILES conf scripts package.tgz

clean:
	rm -f package.tgz Medio-*.spk package/*.pyc
